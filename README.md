# Pygments Lexer JSON with comments

This is a unofficial lexer for pygments (python 2) which supports JSON files with comments (for e.g. a paper)

# Installation
1. Start at your python directory and go to ```Lib\site-packages\pygments\lexers```
2. Place the [json_lexer_comments.py](./json_lexer_comments.py) into the directory
3. Run 
``` user$> python .\_mapping.py ```

Note: To enable usage with minted use python 2! 
The extra file in the repo can be highlighted with the lexer and demonstrates it's effect.
